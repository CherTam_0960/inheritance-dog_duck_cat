/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.inheritance;

/**
 *
 * @author kitti
 */
public class Animal {

    protected String name;
    private String color;
    protected int numberOfLeg = 0;

    public Animal(String name, String color, int numberOfleg) {
        System.out.println("Animal created.");
        this.name = name;
        this.color = color;
        this.numberOfLeg = numberOfleg;
    }

    public void walk() {
        System.out.println("-Animal walk-");
    }

    public void speak() {
        System.out.println("-Animal speak-");
        System.out.println("My name is " + this.name + ". " + "\nMy color is "
                + this.color + ". \nNumber Of leg is " + this.numberOfLeg + ".");
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public int getNumberOfLeg() {
        return numberOfLeg;
    }
    
}
